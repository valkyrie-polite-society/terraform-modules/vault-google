seal "transit" {
  address     = "${address}"
  tls_ca_cert = "/etc/vault/ca.pem"
  token       = "${token}"
  key_name    = "${key_name}"
  mount_path  = "${mount_path}"
}
