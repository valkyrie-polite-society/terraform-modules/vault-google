storage "raft" {
  path    = "/var/lib/vault"
  node_id = "${hostname}"

  retry_join {
    auto_join = "provider=gce project_name=${project} tag_value=${auto_join_tag}"
    
    leader_ca_cert_file     = "/etc/vault/ca.pem"
    leader_client_cert_file = "/etc/vault/tls.crt"
    leader_client_key_file  = "/etc/vault/tls.key"
  }
}
