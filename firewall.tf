resource "google_compute_firewall" "basic" {
  name        = "${var.cluster_name}-basic"
  network     = var.network
  description = "Allows ingress on the HTTPS port and the API cluster port from any user-provided CIDRs."

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["8200", "8201"]
  }

  source_ranges = flatten([
    var.allowed_ip_cidrs
  ])

  source_tags = [
    local.network_tag,
  ]
}

locals {
  network_tag = "${var.cluster_name}-https"
}
