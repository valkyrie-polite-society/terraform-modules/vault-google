resource "google_storage_bucket" "snapshots" {
  name          = "${var.cluster_name}-vault-snapshots"
  location      = "US"
  force_destroy = true

  uniform_bucket_level_access = false

  lifecycle_rule {
    condition {
      age                        = var.snapshot_keep_days
      days_since_custom_time     = 0
      days_since_noncurrent_time = 0
      matches_storage_class      = []
      num_newer_versions         = 0
      with_state                 = "ANY"
    }
    action {
      type = "Delete"
    }
  }
}

resource "google_storage_bucket_iam_member" "snapshots" {
  bucket = google_storage_bucket.snapshots.name
  role   = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.vault.email}"
}
