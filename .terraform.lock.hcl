# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/adriennecohea/vaultutility" {
  version     = "0.0.3"
  constraints = ">= 0.0.3"
  hashes = [
    "h1:gN/zaIl36uUoJijSw3U8Dfwjo0C5OVYsWHu0a2yTSNg=",
    "zh:093570033389a3c98882afaa9a7ce321265c89e8a66923fd4085381796dd9d7f",
    "zh:14546648024e1cc3638a818eebc4dbb0ac152d940875d8b08dc98ea68f01ba91",
    "zh:4524217a434f8084bbec064c5ccbeba36a5e786ccb53dea70897164a200add36",
    "zh:461bef387dc181faba602900ce241c6f5cddf66e242e064b3265d8c091d36b29",
    "zh:552e2805bee0747793e516868001dd7cfce97a712e90fa2f8904c32b6c953661",
    "zh:5623ce07284ba752fc8e0d52efbc15864f51a4e95fe8480fe74d1581163d794f",
    "zh:58199d8db173de6265472853839703e8f0976184d45cd6ccc9fcc617ca23f06e",
    "zh:5a27113e3938a6c5a47ed214983eea0b1bed54151c3e3e1b4cbd24c864ffaf14",
    "zh:9f3fdd1423f165ab8379036793466111978e890d5d7a5e73a8a7204c79e98e8b",
    "zh:b37e123794a6c64c19adbf917f874b27b0f52afc041cee5d77060e767e4a48b9",
    "zh:d6164d78dfdaad684df150d405e487d8e45eb5c6df70bf8a00bab58e0d61b52b",
  ]
}

provider "registry.terraform.io/hashicorp/google" {
  version     = "3.78.0"
  constraints = "~> 3.78.0"
  hashes = [
    "h1:iCyTW8BWdr6Bvd5B89wkxlrB8xLxqHvT1CPmGuKembU=",
    "zh:027971c4689b6130619827fe57ce260aaca060db3446817d3a92869dba7cc07f",
    "zh:0876dbecc0d441bf2479edd17fe9141d77274b5071ea5f68ac26a2994bff66f3",
    "zh:2a5363ed6b1b880f5284e604567cfdabecca809584c30bbe7f19ff568d1ea4cd",
    "zh:2f5af69b70654bda91199f6393253e3e479107deebfeddc3fe5850b3a1e83dfb",
    "zh:52e6816ef11f5f799a6626dfff384e2153b37450d8320f1ef1eee8f71a2a87b2",
    "zh:59ae534607db13db35c0015c06d1ae6d4886f01f7e8fd4e07bc120236a01c494",
    "zh:65ab2ed1746ea02d0b1bbd8a22ff3a95d09dc8bdb3841fbc17e45e9feccfb327",
    "zh:877a71d24ff65ede3f0c5973168acfeaea0f2fea3757cab5600efcddfd3171d5",
    "zh:8b10c9643a4a53148f6758bfd60804b33c2b838482f2c39ed210b729e6b1e2e8",
    "zh:ba682648d9f6c11a6d04a250ac79eec39271f615f3ff60c5ae73ebfcc2cdb450",
    "zh:e946561921e0279450e9b9f705de9354ce35562ed4cc0d4cd3512aa9eb1f6486",
  ]
}

provider "registry.terraform.io/hashicorp/tls" {
  version     = "2.2.0"
  constraints = "~> 2.2.0"
  hashes = [
    "h1:BRvNNW/32RAFXRcEAovtJWVbFt8zesIKkQm2N0GBCn8=",
    "zh:0e99d1b3710f30df818cd662e2ea7b2005cec2a0c64cedd31c459b214f45e9b5",
    "zh:43a97fd341c66113aeecfa2f976eb8f5e0b83a6a8824828fac2afef2682d9957",
    "zh:501aa0ed294c7befb8bf6e00fd9d8cf4055e1135cb8872f83488ebcde9a89985",
    "zh:51862aeed0f28092c877a5f64ddac55b8f6c05cf5278fb51afab5d20b3819934",
    "zh:52c22bf8621a120080c7c8c11bfab51678ab9dc3e5c88dc89df8b2c0434a8c89",
    "zh:658cce07951f8ba8f170b71198c198e526a82ba56cb74d27b24adf2574eb07b2",
    "zh:80a7db37b00c5d9054f68dc62283ce11dcc83b714d550fc56e6b8544bc01d1a8",
    "zh:935dd4f4995286c871162bb96ebb49c2d80ef09f2225be62a4ef06c0fcbd72d4",
    "zh:af89f57dc41c4d09fd9b7d1277e5ad1d4989cd672f3e58e1891d59020015d21a",
    "zh:d45870cf06ed9910d5956175c996cc7bb677f3a8edd94c66a48a3fb93c8d2a84",
    "zh:de96c5fadb346adb5e39ea8901c93335b0f3408dd8d477a996b4eb845a8d2343",
    "zh:e0f3c179975b7fa5898ebe9cc70621f9da301a14e5262399b074ccb675308bd3",
  ]
}
