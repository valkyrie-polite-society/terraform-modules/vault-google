resource "google_compute_instance" "vault" {
  count = var.cluster_size

  name         = "${var.cluster_name}-${count.index}"
  machine_type = var.machine_type
  zone         = var.zone

  tags = [var.cluster_name]

  boot_disk {
    initialize_params {
      image = data.google_compute_image.vault.self_link
    }
  }

  attached_disk {
    source      = google_compute_disk.vault[count.index].self_link
    device_name = "vault"
  }

  network_interface {
    network    = var.network
    network_ip = google_compute_address.private[count.index].address

    access_config {
      nat_ip = google_compute_address.public[count.index].address
    }
  }

  scheduling {
    preemptible       = true
    automatic_restart = false
  }

  metadata = {
    "user-data" = templatefile("${path.module}/cloud-init.yaml", {

      # Vault server configuration

      vault_ca_pem_b64  = base64encode(data.terraform_remote_state.ca.outputs.root_ca_certificate_pem)
      vault_tls_key_b64 = base64encode(tls_private_key.vault[count.index].private_key_pem)
      vault_tls_crt_b64 = base64encode(join("\n", [
        tls_locally_signed_cert.vault[count.index].cert_pem,
        data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem,
        data.terraform_remote_state.ca.outputs.root_ca_certificate_pem,
      ]))
      storage_config_b64 = base64encode(templatefile("${path.module}/templates/vault/storage.hcl", {
        hostname      = "${var.cluster_name}-${count.index}"
        project       = var.project
        auto_join_tag = var.cluster_name
      }))
      addresses_config_b64 = base64encode(templatefile("${path.module}/templates/vault/addresses.hcl", {
        api_addr     = "https://${google_compute_address.private[count.index].address}:8200"
        cluster_addr = "https://${google_compute_address.private[count.index].address}:8201"
      }))
      seal_config_b64 = base64encode(templatefile("${path.module}/templates/vault/seal-gcpckms.hcl", {
        project    = local.kms_key_ring.project
        region     = local.kms_key_ring.location
        key_ring   = local.kms_key_ring.name
        crypto_key = local.kms_crypto_key.name
      }))
      ui_b64           = base64encode(templatefile("${path.module}/templates/vault/ui.hcl", {}))
      registration_b64 = base64encode(templatefile("${path.module}/templates/vault/registration.hcl", {}))

      # Consul agent configuration

      consul_ca_pem_b64  = base64encode(data.terraform_remote_state.ca.outputs.root_ca_certificate_pem)
      consul_tls_key_b64 = base64encode(tls_private_key.consul[count.index].private_key_pem)
      consul_tls_crt_b64 = base64encode(join("\n", [
        tls_locally_signed_cert.consul[count.index].cert_pem,
        data.terraform_remote_state.ca.outputs.intermediate_ca_certificate_pem,
        data.terraform_remote_state.ca.outputs.root_ca_certificate_pem,
      ]))
      consul_gossip_b64 = base64encode(jsonencode({ encrypt = data.terraform_remote_state.consul.outputs.gossip_key }))
      retry_join_b64 = base64encode(jsonencode({
        retry_join = ["provider=gce project_name=${var.project} tag_value=${var.consul_auto_join_tag}"]
      }))
      datacenter_b64 = base64encode(jsonencode({
        datacenter = var.consul_datacenter
      }))
      acl_b64 = base64encode(templatefile("${path.module}/templates/consul/acl.hcl", {
        consul_acl_token = data.terraform_remote_state.consul.outputs.root_token
      }))
    })
  }

  service_account {
    email  = google_service_account.vault.email
    scopes = ["cloud-platform"]
  }
}
